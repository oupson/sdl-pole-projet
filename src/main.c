#include <stdio.h>
#include <SDL2/SDL.h>

#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

typedef struct Character {
    float x;
    float y;
    int width;
    int height;
} Character_t;

typedef struct Game {
    int width;
    int height;
    SDL_Window *window;
    SDL_Renderer *renderer;
    Character_t character;
} Game_t;

Game_t *create_game_t(int width, int height) {
    Game_t *game = calloc(1, sizeof(Game_t));

    game->width = width;
    game->height = height;

    game->character.x = (float) width / 2;
    game->character.y = (float) height / 2;

    game->character.width = 100;
    game->character.height = 100;

    game->window = SDL_CreateWindow(
            "SDL2 Demo",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            width, height,
            SDL_WINDOW_FULLSCREEN
    );

    game->renderer = SDL_CreateRenderer(game->window, -1, NULL);

    return game;
};

void destroy_game_t(Game_t *game) {
    SDL_DestroyWindow(game->window);
    SDL_DestroyRenderer(game->renderer);
}


void update_game_t(Game_t *game) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                destroy_game_t(game);
                SDL_Quit();
                exit(0);
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    destroy_game_t(game);
                    SDL_Quit();
                    exit(0);
                }
                break;
        }
    }
}

void render_game_t(Game_t *game) {
    SDL_SetRenderDrawColor(game->renderer, 0xff, 0xff, 0xff, 0xff);
    SDL_RenderClear(game->renderer);

    SDL_SetRenderDrawColor(game->renderer, 0xc1, 0xc1, 0xc1, 0xff);

    for (int x = 0; x < game->width; x += 25) {
        SDL_RenderDrawLine(game->renderer, x, 0, x, game->height);
    }


    for (int y = 0; y < game->height; y += 25) {
        SDL_RenderDrawLine(game->renderer, 0, y, game->width, y);
    }

    SDL_Rect rect = {.x = (int) game->character.x - (game->character.width / 2), .y = (int) game->character.y -
                                                                                      (game->character.height /
                                                                                       2), .w = game->character.width, .h = game->character.height};
    SDL_SetRenderDrawColor(game->renderer, 0xff, 0x0, 0x0, 0xff);
    SDL_RenderFillRect(game->renderer, &rect);

    SDL_RenderPresent(game->renderer);
}



void tick_game_t(Game_t *game) {
    int keylen;
    const unsigned char *keyboard = SDL_GetKeyboardState(&keylen);

    if (keyboard[SDL_SCANCODE_W] || keyboard[SDL_SCANCODE_UP]) {
        game->character.y = MAX(game->character.y - 2, game->character.height / 2);
    }
    if (keyboard[SDL_SCANCODE_S] || keyboard[SDL_SCANCODE_DOWN]) {
        game->character.y = MIN(game->character.y + 2, game->height - (game->character.height / 2));
    }
    if (keyboard[SDL_SCANCODE_A] || keyboard[SDL_SCANCODE_LEFT]) {
        game->character.x = MAX(game->character.x - 2, game->character.width / 2);
    }
    if (keyboard[SDL_SCANCODE_D] || keyboard[SDL_SCANCODE_RIGHT]) {
        game->character.x = MIN(game->character.x + 2, game->width - (game->character.width / 2));
    }
}

void run_game_t(Game_t *game) {
    while (1) {
        update_game_t(game);
        tick_game_t(game);
        render_game_t(game);
    }
}

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    } else {
        Game_t *game = create_game_t(SCREEN_WIDTH, SCREEN_HEIGHT);

        run_game_t(game);
    }
    SDL_Quit();
    return 0;
}
